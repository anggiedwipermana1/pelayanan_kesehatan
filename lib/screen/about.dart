import 'dart:ui';
import 'package:flutter/material.dart';

class Aboutpage extends StatefulWidget {
  Aboutpage({Key key}) : super(key: key);

  @override
  _AboutpageState createState() => _AboutpageState();
}

class _AboutpageState extends State<Aboutpage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text("MITRA PLUMBON"),),
      drawer: Drawer(),
      body: SafeArea(child: Container(
        alignment: Alignment.center,
        margin: EdgeInsets.only(top:30, left:20, right:20),
       child: Column(
         children: [
           Container(
             child: Image.asset("assets/images/1.jpg")
           ),
           SizedBox(
             height: 20,
           ),
           Container(
             child: Text("Aplikasi ini adalah pelayanan kesehatan secara online berbasis Android yang bisa mempermudah masyarakat untuk konsultasi atau berobat ke dokter spesialis yang dibutuhkan. ",style: TextStyle(fontSize: 18, ),textAlign: TextAlign.center,)
           ),
           SizedBox(
             height: 300,
           ),
           Container(
             alignment: Alignment.bottomCenter,
            //  color: Colors.black45,
             padding: EdgeInsets.all(21),
             child: Column(
               children: [
                 _copyright("© Anggie Dwi Permana"),
                 _copyright("18282002"),
                 _copyright("2021")

               ],
             ),
           )
         ],),
    ),)
    );
  }

  Widget _copyright(String data){
    return Text(data, style: TextStyle(fontSize: 20,fontWeight: FontWeight.bold),);
  }
}