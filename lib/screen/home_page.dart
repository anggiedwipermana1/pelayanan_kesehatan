import 'package:flutter/material.dart';
import 'package:mitra_plumbon/object/mitra_item_object.dart';
import 'package:mitra_plumbon/screen/about.dart';
import 'package:mitra_plumbon/screen/entryform.dart';
import 'package:mitra_plumbon/profile.dart';
import 'login_screen.dart';
import 'package:flutter/cupertino.dart';


class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override    
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage>
    with SingleTickerProviderStateMixin {
  int selectedPage = 0;
  TabController _tabController;
  PageController _pageController;
  


  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  bool isSearching = false;

  List<MitraItemObject> doctors = [

    MitraItemObject(
        imageName: "assets/images/dranak.png",
        doctorName: "dr. Indra, M.Kes, Sp.A",
        specialisName: "Dokter Anak",
        description:
            "Menjadi seorang Dokter Spesialis Anak di Rumah Sakit Mitra Keluarga Gading Serpong, dr. Indra, M.Kes, Sp.A telah menyelesaikan pendidikan Spesialis Anak dan Magister Kesehatan di Universitas Hasanuddin, Makassar pada tahun 2017 (Combine Degree). Beberapa pengalaman kerja beliau diantaranya pernah berpraktik di RSUD Cendrawasih Dobo, Puskesmas Modo, RSU Deli, dan menjadi Asisten Dosen di FK Universitas Kristen Maranatha. r. Indra terhimpun dalam Ikatan Dokter Anak Indonesia dan dapat membantu memberikan bantuan pelayanan medis terkait kesehatan anak.",
        backgroudColor: Colors.transparent),
    MitraItemObject(
        imageName: "assets/images/drgigi.png",
        doctorName: "drg. Sima Novrita Deviyanti",
        specialisName: "Dokter Gigi",
        description:
            "drg. Sima Novrita Deviyanti adalah seorang Dokter Gigi. Saat ini drg. Sima Novrita Deviyanti berpraktik di RS Harapan Keluarga Jababeka dan RS Anna Pekayon, Bekasi sebagai Dokter Gigi. Beliau dapat memberikan layanan Konsultasi keluhan gigi umum. drg. Sima Novrita Deviyanti menamatkan pendidikan Kedokteran Gigi di Faklutas Kedokteran Universita Indonesia. Selain menjadi seorang Dokter Gigi, beliau juga tergabung Persatuan Dokter Gigi Indonesia (PDGI).",
        backgroudColor: Colors.transparent),
    MitraItemObject(
        imageName: "assets/images/drkulit.png",
        doctorName: "dr. Eltriarni Bromiyanti, Sp.KK",
        specialisName: "Dokter Kulit",
        description:
            "dr. Eltriarni Bromiyanti, Sp.KK merupakan seorang Dokter Spesialis Kulit dan Kelamin yang telah menamatkan pendidikan Spesialis Kulit dan Kelamin di Fakultas Kedokteran Universitas Diponegoro. Beliau tergabung dalam Ikatan Dokter Indonesia (IDI) dan Perhimpunan Dokter Spesialis Kulit dan Kelamin Indonesia(PERDOSKI). r. Eltriarni saat ini melakukan praktik di RS Mulia Pajajaran, Bogor sebagai Dokter Kulit dan Kelamin. Adapun layanan yang diberikan meliputi : Konsultasi perihal kesehatan kulit dan kelamin.",
        backgroudColor: Colors.transparent)
  ];

  @override
  void initState() {
    super.initState();
    _tabController = TabController(initialIndex: 0, length: 7, vsync: this);
    _pageController = PageController(initialPage: 0, viewportFraction: 0.8);
  }

  _makeOrder() {
    // Navigator.push(context, MaterialPageRoute(builder: (_) => OrderScreen()));
  }

  _logOut() {
    Navigator.pushReplacement(
        context, MaterialPageRoute(builder: (_) => LoginScreen()));
  }

  _searchPressed() {
    isSearching = !isSearching;
    setState(() {});
  }

  Widget _stockSelector(int index) {
    return AnimatedBuilder(
      animation: _pageController,
      builder: (BuildContext context, Widget widget) {
        double value = 1;
        if (_pageController.position.haveDimensions) {
          value = _pageController.page - index;
          value = (1 - (value.abs() * 0.3)).clamp(0.0, 1.0);
        }
        return Center(
          child: SizedBox(
            height: Curves.easeInOut.transform(value) * 500,
            width: Curves.easeInOut.transform(value) * 400,
            child: widget,
          ),
        );
      },
      child: GestureDetector(
        onTap: () {
          // Navigator.push(context,
          //     FadeRoute(page: MitraDetailScreen(Mitra: stock[selectedPage])));
        },
        child: Stack(
          children: <Widget>[
            Container(
              height: 400,
              margin: EdgeInsets.only(left: 20, right: 20),
              decoration: BoxDecoration(
                  gradient: LinearGradient(
                      colors: [Colors.deepOrangeAccent[100], Colors.deepOrangeAccent], stops: [0, 1]),
                  borderRadius: BorderRadius.circular(25)),
              child: Stack(
                children: <Widget>[
                  Center(
                    child: Hero(
                      tag: doctors[index].imageName,
                      child: Image(
                        fit: BoxFit.cover,
                        image: AssetImage(doctors[index].imageName),
                      ),
                    ),
                  ),

                  Positioned(
                    bottom: 25,
                    left: 20,
                    child: Column(
                      children: <Widget>[
                        Text(doctors[index].doctorName, 
                            style: TextStyle(
                                color: Colors.white,
                                fontSize: 25,
                                fontWeight: FontWeight.bold)),
                  Positioned(
                    bottom: 25,
                    left: 20,
                    child: Column(
                      children:<Widget>[
                        Text(doctors[index].specialisName,
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 25,
                            fontWeight: FontWeight.bold)),
                          ],
                         )
                       ,)
                      ],
                    ),
                  ),
                ],
              ),
            ),
            
            Positioned(
              bottom: 10,
              left: 10,
              right: 10,
              child: RawMaterialButton(
                padding: EdgeInsets.all(15),
                child: Icon(
                  Icons.chat,
                  size: 30,
                  color: Colors.white,
                ),
                fillColor: Colors.black,
                shape: CircleBorder(),
                elevation: 2.0,
                onPressed: () => _makeOrder(),
              ),
            )
          ],
        ),
      ),
    );
  }

  _mainWidgetSwitcher(bool isSearching) {
    return !isSearching
        ? Column(
            children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Hero(
                    tag: "drawer_button",
                    child: Padding(
                      padding: const EdgeInsets.only(left: 25.0),
                      child: IconButton(
                          icon: Icon(Icons.menu, size: 30),
                          onPressed: () =>
                              _scaffoldKey.currentState.openDrawer()),
                    ),
                  ),
                  Padding(
                      padding: const EdgeInsets.only(right: 25.0),
                      child: IconButton(
                          icon: Icon(
                            Icons.search,
                            size: 30,
                          ),
                          onPressed: () => _searchPressed()))
                ],
              ),
              Container(
                padding: const EdgeInsets.all(20.0),
                child: Text(
                  "Buat Janji Mudah & Cepat",
                  style: TextStyle(fontWeight: FontWeight.w600, fontSize: 30),
                ),
              ),
              Container(
                padding: const EdgeInsets.all(0.0),
                child: Text(
                  "Layanan Booking Dokter Terbaik",
                  style: TextStyle(fontWeight: FontWeight.w500, fontSize: 20), 
                  ),
                ),

              TabBar(
                controller: _tabController,
                indicatorColor: Colors.transparent,
                labelColor: Colors.black,
                unselectedLabelColor: Colors.grey.withOpacity(0.6),
                labelPadding: EdgeInsets.symmetric(horizontal: 35),
                isScrollable: true,
                tabs: <Widget>[
                  Tab(
                    child: Text(
                      "Chat Bersama Dokter",
                      style:
                          TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
                    ),
                  ),
                  Tab(
                    child: Text(
                      "Cari Dokter",
                      style:
                          TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
                    ),
                  ),
                  Tab(
                    child: Text(
                      "Proteksi Pelayanan",
                      style:
                          TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
                    ),
                  ),
                  Tab(
                    child: Text(
                      "SHOP",
                      style:
                          TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
                    ),
                  ),
                  Tab(
                    child: Text(
                      "Jadwal",
                      style:
                          TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
                    ),
                  ),
                  Tab(
                    child: Text(
                      "Alamat",
                      style:
                          TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
                    ),
                  ),
                  Tab(
                    child: Text(
                      "Artikel Kesehatan",
                      style:
                          TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
                    ),
                  ),
                ],
              ),
              SizedBox(
                height: 20,
              ),
              Container(
                height: 450,
                width: double.infinity,
                child: PageView.builder(
                  controller: _pageController,
                  onPageChanged: (int index) {
                    setState(() {
                      this.selectedPage = index;
                    });
                  },
                  itemCount: doctors.length,
                  itemBuilder: (BuildContext context, int index) {
                    return _stockSelector(index);
                  },
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text("Description:",
                    style: TextStyle(fontWeight: FontWeight.bold)),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text(doctors[selectedPage].description),
              )
            ],
          )
        : Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Row(
                children: <Widget>[
                  Expanded(
                    child: Padding(
                      padding: const EdgeInsets.only(left: 20.0),
                      child: TextField(
                        onSubmitted: (String text) {
                          if (text.length == 0) {
                            _searchPressed();
                          }
                        },
                        autofocus: true,
                        decoration: InputDecoration(
                            border: InputBorder.none,
                            hintText: 'Ada yang bisa kami bantu?'),
                      ),
                    ),
                  ),
                  Padding(
                      padding: const EdgeInsets.only(right: 25.0),
                      child: IconButton(
                          icon: Icon(
                            Icons.search,
                            size: 30,
                          ),
                          onPressed: () => _searchPressed())),
                ],
              ),
              InkWell(
                onTap: () {
                  // Navigator.push(
                  //     context,
                  //     MaterialPageRoute(
                  //         builder: (_) =>
                  //             MitraDetailScreen(Mitra: doctors[selectedPage])));
                },
                child: Padding(
                  padding: const EdgeInsets.only(left: 15.0, top: 20),
                  child: Container(
                      width: double.infinity, child: Text("Dokter Anak")),
                ),
              ),
              InkWell(
                child: Padding(
                  padding: const EdgeInsets.only(left: 15.0, top: 20),
                  child: Container(
                      width: double.infinity, child: Text("Dokter Gigi")),
                ),
              ),
              InkWell(
                onTap: () {
                  // Navigator.push(
                  //     context,
                  //     MaterialPageRoute(
                  //         builder: (_) =>
                  //             MitraDetailScreen(Mitra: doctor[selectedPage])));
                },
                child: Padding(
                  padding: const EdgeInsets.only(left: 15.0, top: 20),
                  child: Container(
                      width: double.infinity, child: Text("Dokter Kulit")),
                ),
              ),
            ],
          );
  }

  Widget stockSelector(int index) => _stockSelector(index);

  @override
  Widget build(BuildContext context) => Scaffold(
        key: _scaffoldKey,
        body: Container(
          width: double.infinity,
          child: SafeArea(
            child: SingleChildScrollView(
              child: Column(
                children: <Widget>[
                  AnimatedSwitcher(
                      duration: Duration(milliseconds: 300),
                      child: _mainWidgetSwitcher(isSearching))
                ],
              ),
            ),
          ),
        ), // This
        drawer: Drawer(
          elevation: 10,
          child: Container(
            decoration: BoxDecoration(
                gradient: LinearGradient(
                    colors: [Colors.deepOrangeAccent, Colors.deepOrangeAccent[100]],
                    stops: [0, 1],
                    begin: Alignment.topCenter,
                    end: Alignment.bottomCenter)),
            child: SafeArea(
                child: Stack(
              children: <Widget>[
                Hero(
                  tag: "drawer_button",
                  child: Padding(
                    padding: const EdgeInsets.only(left: 25.0),
                    child: IconButton(
                        icon: Icon(Icons.arrow_back, size: 30),
                        onPressed: () => Navigator.of(context).pop()),
                  ),
                ),
                Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Center(
                        child: Container(
                      width: double.infinity,
                      height: 150,
                      child: Stack(
                        children: <Widget>[
                          // Positioned(
                          //   bottom: 50,
                          //   child: Container(
                          //     color: Colors.black,
                          //     width: 400,
                          //     height: 150,
                          //   ),
                          // ),
                          Center(
                            child: Container(
                              height: 150,
                              width: 150,
                              decoration: BoxDecoration(
                                  shape: BoxShape.circle,
                                  image: new DecorationImage(
                                    fit: BoxFit.cover,
                                    image:AssetImage(
                                      'assets/images/2.png',
                                    ),
                                  )),
                            ),
                          ),
                        ],
                      ),
                    )),
                    // Center(
                    //   child: Column(
                    //       crossAxisAlignment: CrossAxisAlignment.start,
                    //       children: <Widget>[
                    //         Text("Anggie Dwi Permana"),
                    //         Text("18282002")
                    //       ]),
                    // ),
                    Padding(
                      padding: EdgeInsets.all(10),
                      child: InkWell(
                        onTap: () {
                          _switchToCalendar();
                        },
                        child: Container(
                          decoration: BoxDecoration(
                              color: Colors.white,
                              borderRadius:
                                  BorderRadius.all(Radius.circular(25))),
                          child: Padding(
                            padding: const EdgeInsets.all(15.0),
                            child: Center(
                                child: Text("About",
                                    style: TextStyle(color: Colors.grey))),
                          ),
                        ),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.all(10),
                      child: InkWell(
                        onTap: (){
                          Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (_) =>
                      Entryform()));
                        },
                        child: Container(
                          decoration: BoxDecoration(
                              color: Colors.white,
                              borderRadius:
                                  BorderRadius.all(Radius.circular(25))),
                          child: Padding(
                            padding: const EdgeInsets.all(15.0),
                            child: Center(
                                child: Text("Input Data",
                                    style: TextStyle(color: Colors.grey))),
                          ),
                        ),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.all(10),
                      child: InkWell(
                        onTap: (){
                          Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (_) =>
                      Profile()));
                        },
                      child: Container(
                        decoration: BoxDecoration(color: Colors.transparent),
                        child: Padding(
                          padding: const EdgeInsets.all(15.0),
                          child: Center(
                              child: Text("Profile",
                                  style: TextStyle(color: Colors.white))),
                        ),
                      ),
                    ),
                    ),
                    InkWell(
                      onTap: () => _logOut(),
                      child: Container(
                        decoration: BoxDecoration(color: Colors.transparent),
                        child: Padding(
                          padding: const EdgeInsets.all(15.0),
                          child: Center(
                              child: Text("Logout",
                                  style: TextStyle(color: Colors.white))),
                        ),
                      ),
                    ),
                  ],
                ),
              ],
            )),
          ),
        ) // trailing comma makes auto-formatting nicer for build methods.
        );
  _switchToCalendar(){
    // Navigator.of(context).push(
    //   FadeRoute(
    //     page: CalendarScreen(),
    //   ),
    // );
     Navigator.push(context, MaterialPageRoute(builder: (_)=>Aboutpage()));
     Navigator.push(context, MaterialPageRoute(builder: (_)=>Profile()));
     
  }
}
