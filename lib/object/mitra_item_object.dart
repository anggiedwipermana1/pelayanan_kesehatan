import 'package:flutter/material.dart';

class MitraItemObject {
  String imageName;
  String doctorName;
  String specialisName;

  String description;
  Color backgroudColor;

  MitraItemObject( {
    this.imageName,
    this.doctorName,
    this.specialisName,
    this.description = "",
    this.backgroudColor = Colors.deepOrange
  });
}